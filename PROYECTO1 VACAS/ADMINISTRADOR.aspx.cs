﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PROYECTO1_VACAS
{
    public partial class ADMINISTRADOR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("formulariousuariogeneral.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("nodificarusuario.aspx");
        }

        protected void Button10_Click(object sender, EventArgs e)
        {
            Response.Redirect("VEHICULO.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("asignacionvehiculo.aspx");
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            Response.Redirect("categoria.aspx");
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormularioProducto.aspx");
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("FormularioProducto.aspx");
        }
    }
}