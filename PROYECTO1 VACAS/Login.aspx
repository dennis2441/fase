﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="PROYECTO1_VACAS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #3a5a5e;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}

.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}

body {
  background: #76b852;=
}


    </style>
</head>
<body>
 <div class="login-page">
  <div class="form">

    <form class="login-form"  runat="server">
        <h1>Login</h1>
      <asp:TextBox ID="USERN" class="form-control" runat="server" Height="15px" placeholder="Username" ></asp:TextBox>
   <asp:TextBox ID="PASS" TextMode="Password" class="form-control" runat="server" Height="15px" placeholder="Password" ></asp:TextBox>&nbsp;

        <asp:Label cssclass="yourclass"  runat="server" Width="160px">Tipo</asp:Label>
         
       <asp:DropDownList ID="DropDownList2" runat="server">
           <asp:ListItem>ADMINISTRADOR</asp:ListItem>
             <asp:ListItem>SUPERVISOR</asp:ListItem>
           <asp:ListItem>GERENTE</asp:ListItem>
             <asp:ListItem>VENDEDOR</asp:ListItem>
        </asp:DropDownList>&nbsp;<br /><br />

        <asp:Button ID="BUTT" runat="server" Text="ENTRAR" OnClick="Button1_Click" BackColor="#3399FF" BorderColor="#66FF66"/>
	  <asp:Button ID="Button6" runat="server" Text="Regresar" BackColor="#3399FF" BorderColor="#66FF66"  />

      
   
    </form>
      
  </div>
</div>
</body>
</html>
