﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PROYECTO1_VACAS
{
    public partial class municipio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDA;
            cmd.CommandText = "SELECT  count(IDVEHICULO) FROM VEHICULO";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            sqlDA = new SqlDataAdapter(cmd);
            sqlDA.Fill(dataTable);

            Int32 count = (Int32)cmd.ExecuteScalar();
            count = count + 1;
            numero.Text = count.ToString();
            Session["listas"] = numero.Text.ToString();
        }
        private void MsgBox(string sMessage)
        {
            Response.Write("<script>alert('" + sMessage + "')</script>");
        }
        protected void BUTT_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrEmpty(muni.Text.ToString()))
            {
                MsgBox("Introdusca un nombre");

            }
            else
            {
                SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand();
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDA;
                cmd.CommandText = "insert into DEPARTAMENTO(NOMBRE) values('" + muni.Text.ToString() + "')";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conexion;
                sqlDA = new SqlDataAdapter(cmd);
                sqlDA.Fill(dataTable);
                Response.Redirect("ADMINISTRADOR.aspx");
            }

        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            Response.Redirect("ADMINISTRADOR.aspx");
        }
    }
}