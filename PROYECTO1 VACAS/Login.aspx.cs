﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PROYECTO1_VACAS
{
    public partial class Login : System.Web.UI.Page
    {
        SqlCommand cmd;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private void MsgBox(string sMessage)
        {
            Response.Write("<script>alert('" + sMessage + "')</script>");
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

            string comp = DropDownList2.SelectedItem.Value;

            if (comp == "ADMINISTRADOR")
            {
           
                if (USERN.Text.Equals("adm") && PASS.Text.Equals("adm"))
                {
                    Response.Redirect("ADMINISTRADOR.aspx");
                }
                else
                {
                    MsgBox("DATOS EQUIVOCADOS");
                            
                }

            }
            else if (comp == "VENDEDOR")
            {
                using (SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True"))
                {
                    sqlCon.Open();
                    int nit = int.Parse(USERN.Text.ToString());
                    string query = "SELECT COUNT(1) FROM EMPLEADO WHERE NIT=@username AND PASSWORD=@Password AND IDPUESTO=@TIPO";
                    cmd = new SqlCommand(query, sqlCon);
                    cmd.Parameters.AddWithValue("@username", nit);

                    cmd.Parameters.AddWithValue("@Password", PASS.Text.Trim());
                    cmd.Parameters.AddWithValue("@TIPO", 1);
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    if (count == 1)
                    {

                      
                        Response.Redirect("VENDEDOR.aspx");


                    }
                    else
                    {
                        MsgBox("DATOS EQUIVOCADOS");
                    }
                }
            }
            else if (comp == "SUPERVISOR")
            {
                using (SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True"))
                {
                    sqlCon.Open();
                    int nit = int.Parse(USERN.Text.ToString());
                    string query = "SELECT COUNT(1) FROM EMPLEADO WHERE NIT=@username AND PASSWORD=@Password AND IDPUESTO=@TIPO";
                    cmd = new SqlCommand(query, sqlCon);
                    cmd.Parameters.AddWithValue("@username", nit);

                    cmd.Parameters.AddWithValue("@Password", PASS.Text.Trim());
                    cmd.Parameters.AddWithValue("@TIPO", 2);
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    if (count == 1)
                    {


                        Response.Redirect("Supervisor.aspx");


                    }
                    else
                    {
                        MsgBox("DATOS EQUIVOCADOS");
                    }
                }
            }
            else if (comp == "GERENTE")
            {
                using (SqlConnection sqlCon = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True"))
                {
                    sqlCon.Open();
                    int nit = int.Parse(USERN.Text.ToString());
                    string query = "SELECT COUNT(1) FROM EMPLEADO WHERE NIT=@username AND PASSWORD=@Password AND IDPUESTO=@TIPO";
                    cmd = new SqlCommand(query, sqlCon);
                    cmd.Parameters.AddWithValue("@username", nit);

                    cmd.Parameters.AddWithValue("@Password", PASS.Text.Trim());
                    cmd.Parameters.AddWithValue("@TIPO", 3);
                    int count = Convert.ToInt32(cmd.ExecuteScalar());
                    if (count == 1)
                    {


                        Response.Redirect("GERENTE.aspx");


                    }
                    else
                    {
                        MsgBox("DATOS EQUIVOCADOS");
                    }
                }
            }
        }
    }
    }
