﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PROYECTO1_VACAS
{
    public partial class formulariousuariogeneral : System.Web.UI.Page
    {
        SqlCommand cmd = new SqlCommand();
        DataTable dataTable = new DataTable();
        SqlDataAdapter sqlDA;
    
        int nitant;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDA;
            cmd.CommandText = "SELECT * FROM EMPLEADO WHERE IDPUESTO=2";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            sqlDA = new SqlDataAdapter(cmd);
            sqlDA.Fill(dataTable);
            SUPE.DataSource = dataTable;
            SUPE.DataTextField = "NOMBRES";

            SUPE.DataBind();
            conexion.Close();
            conexion.Open();
            cmd = new SqlCommand();

            DataTable dataTables = new DataTable();

            cmd.CommandText = "SELECT * FROM EMPLEADO WHERE IDPUESTO=3";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            sqlDA = new SqlDataAdapter(cmd);
            sqlDA.Fill(dataTables);
            GERE.DataSource = dataTables;
            GERE.DataTextField = "NOMBRES";

            GERE.DataBind();



        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string value = tipoempleado.SelectedItem.Value;
                int idp = 0;
                SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
                if (value == "VENDEDOR")
                {
                    idp = 1;
                }
                else if (value == "SUPERVISOR")
                {
                    idp = 2;
                }
                else if (value == "GERENTE")
                {
                    idp = 3;
                }
                Console.WriteLine(idp);

                if (String.IsNullOrEmpty(SG.Text.ToString()))
                {
                   

                    conexion.Open();
                    int nitempl = int.Parse(nitempleado.Text.ToString());
                    int tel = int.Parse(telempleado.Text.ToString());
                    int cel = int.Parse(celularempleado.Text.ToString());

                    //,'" + idcategoria + "'
                    cmd = new SqlCommand();
                    dataTable = new DataTable();

                    cmd.CommandText = "INSERT INTO EMPLEADO(NIT,NOMBRES,APELLIDOS,FECHANACIMIENTO,DIRECCION,TEL,CELULAR,EMAIL,IDPUESTO,PASSWORD) VALUES('" + nitempl + "','" + nombreempleado.Text.ToString() + "','" + APELLIDO.Text.ToString() + "','" + fechanaciemiento.Text.ToString() + "','" + direccionempleado.Text.ToString() + "','" + tel + "','" + cel + "','" + emailempleado.Text.ToString() + "','" + idp + "','" + password.Text.ToString() + "');";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conexion;
                    sqlDA = new SqlDataAdapter(cmd);
                    sqlDA.Fill(dataTable);


                    MsgBox("PRODUCTO AGUARDADO");
                }
                else
                {
                    String jefe = SG.Text.ToString();
                    Console.WriteLine(jefe);
                    conexion.Open();
                    cmd = new SqlCommand();
                    dataTable = new DataTable();

                    cmd.CommandText = "SELECT NIT FROM EMPLEADO WHERE NOMBRES='" + jefe + "'";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conexion;
                    sqlDA = new SqlDataAdapter(cmd);
                    sqlDA.Fill(dataTable);
                    SqlDataReader sdr = cmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        idjefe.Text = sdr["NIT"].ToString();
                    }

                    
                  
                    int idnit = int.Parse(idjefe.Text.ToString());
                    Console.WriteLine(idnit);
                    conexion.Close();



                    conexion.Open();
                    int nitempl = int.Parse(nitempleado.Text.ToString());
                    int tel = int.Parse(telempleado.Text.ToString());
                    int cel = int.Parse(celularempleado.Text.ToString());

                    //,'" + idcategoria + "'
                    cmd = new SqlCommand();
                    dataTable = new DataTable();

                    cmd.CommandText = "INSERT INTO EMPLEADO(NIT,NOMBRES,APELLIDOS,FECHANACIMIENTO,DIRECCION,TEL,CELULAR,EMAIL,IDPUESTO,PASSWORD,NITJE) VALUES('" + nitempl + "','" + nombreempleado.Text.ToString() + "','" + APELLIDO.Text.ToString() + "','" + fechanaciemiento.Text.ToString() + "','" + direccionempleado.Text.ToString() + "','" + tel + "','" + cel + "','" + emailempleado.Text.ToString() + "','" + idp + "','" + password.Text.ToString() + "','" + idnit + "');";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conexion;
                    sqlDA = new SqlDataAdapter(cmd);
                    sqlDA.Fill(dataTable);


                    MsgBox("EMPLEADO AGUARDADO");

                }
              
               

              



            }
            catch (Exception)
            {
                MsgBox("REVISAR DATOS");

            }
        }

        protected void Button4_Click(object sender, EventArgs e)
        {

        }

        protected void GERE_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void SUPE_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            SG.Text = "";
          
            string value = SUPE.SelectedItem.Value;

            SG.Text = value;
            



        }

        protected void Button4_Click1(object sender, EventArgs e)
        {
            SG.Text = "";
        
            string value = GERE.SelectedItem.Value;

            SG.Text = value;
         

           
        }
        private void MsgBox(string sMessage)
        {
            Response.Write("<script>alert('" + sMessage + "')</script>");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Response.Redirect("nodificarusuario.aspx");
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("ADMINISTRADOR.aspx");
        }
    }
}