﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CLIENTE.aspx.cs" Inherits="PROYECTO1_VACAS.CLIENTE" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <title></title>
     
    <style>
        
       
.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #43A047;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}


body {
  background: 	#C0C0C0; 
  
}

    </style>
</head>
<body>

   <div class="login-page">

  <div class="form">
    
    <form class="login-form" runat="server">
        <h1>Empleado</h1>
      <table>
          <tr>
              <td>

                  <asp:Label ID="Label1" runat="server" Text="Clientes Ya registrados"></asp:Label>
              </td>
              <td>
                  <asp:DropDownList ID="tipoempleado" runat="server"></asp:DropDownList>
                  
              </td>
          </tr>
          <tr>
              <td></td>
              <td>
                  <asp:Button ID="Button3" runat="server" Text="ADD" OnClick="Button3_Click" />
              </td>
              
          </tr>

      </table>
  <asp:TextBox  id="nitcliente" placeholder="Nit" value="" runat="server"></asp:TextBox>
        <asp:TextBox  id="nombrecliente" placeholder="Nombre(s)" value="" runat="server"></asp:TextBox>
 <asp:TextBox  id="APELLIDOcliente" placeholder="Apellido(s)" value="" runat="server"></asp:TextBox>
            <asp:TextBox  id="direccion" placeholder="DIreccion" value="" runat="server"></asp:TextBox>
           
            <asp:TextBox  id="telcliente" placeholder="Tel" value="" runat="server"></asp:TextBox>
        <asp:TextBox  id="celular" placeholder="Celular" value="" runat="server"></asp:TextBox>
           <asp:TextBox  id="emailempleado" placeholder="E-mail" value="" runat="server"></asp:TextBox>
         <asp:TextBox  id="limite" placeholder="limite" value="" runat="server"></asp:TextBox>
         <asp:TextBox  id="limitedia" placeholder="Dias Limite" value="" runat="server"></asp:TextBox>
        
        <asp:Label cssclass="yourclass"  runat="server" Width="160px">Tipo </asp:Label>
           <asp:DropDownList ID="tipociudad" runat="server">
           
             
        </asp:DropDownList>&nbsp;<br /><br />
        
           
    
       <asp:TextBox ID="IDC" runat="server" visible="false"></asp:TextBox>
 
    <asp:TextBox ID="listaid" runat="server" visible="false"></asp:TextBox>
         <asp:TextBox ID="numero" runat="server" visible="false"></asp:TextBox>
       
    <asp:Button ID="Button1"  runat="server" Text="Aguardar"  BackColor="#3399FF" BorderColor="#66FF66" OnClick="Button1_Click"  />
        <asp:Button ID="Button2"  runat="server" Text="MODIFICAR USUARIO"  BackColor="#3399FF" BorderColor="#66FF66" OnClick="Button2_Click"  />
        <asp:Button ID="Button4"  runat="server" Text="ELIMINAR USUARIO"  BackColor="#3399FF" BorderColor="#66FF66" OnClick="Button4_Click"   />
    <asp:Button ID="Button8" runat="server" Text="Regresar" BackColor="#3399FF" BorderColor="#66FF66" OnClick="Button8_Click"  />
         
   
    </form>
     
  </div>
</div>
    <p>
        
    </p>
</body>
</html>