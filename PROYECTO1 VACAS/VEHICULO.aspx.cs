﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PROYECTO1_VACAS
{
    public partial class VEHICULO : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;
            SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDA;
            cmd.CommandText = "SELECT  count(IDVEHICULO) FROM VEHICULO";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            sqlDA = new SqlDataAdapter(cmd);
            sqlDA.Fill(dataTable);

            Int32 count = (Int32)cmd.ExecuteScalar();
            count = count + 1;
            numero.Text = count.ToString();
            Session["listas"] = numero.Text.ToString();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            Response.Redirect("ADMINISTRADOR.aspx");
        }
        private void MsgBox(string sMessage)
        {
            Response.Write("<script>alert('" + sMessage + "')</script>");
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection conexion = new SqlConnection(@"Data Source=DESKTOP-EOV3AOU\MSSQLSERVER01; Initial Catalog=vacas; Integrated Security=True");
                conexion.Open();
                string listt = Session["listas"].ToString();
                int list = int.Parse(listt);
                SqlCommand cmd = new SqlCommand();
                DataTable dataTable = new DataTable();
                SqlDataAdapter sqlDA;
                cmd.CommandText = " INSERT INTO VEHICULO(IDVEHICULO,PLACA#,MOTOR#,CHASIS#,MARCA,ESTILO,MODELO) VALUES(" + list + ",'" + placa.Text.ToString() + "','" + motor.Text.ToString() + "','" + chasis.Text.ToString() + "','" + marca.Text.ToString() + "','" + estilo.Text.ToString() + "','" + modelo.Text.ToString() + "');";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = conexion;
                sqlDA = new SqlDataAdapter(cmd);
                sqlDA.Fill(dataTable);
                Page.ClientScript.RegisterStartupScript(this.GetType(), Guid.NewGuid().ToString(), "alert('VEHICULO INGRESADO');window.location='VEHICULO.aspx';", true);
               
            }
            catch (Exception)
            {

                MsgBox("ERROR");
            }
        }
    }
}